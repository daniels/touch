from django.urls import path

from . import views

app_name = 'feedback'
urlpatterns = [
	path('', views.index, name='index'),
	path('done/', views.submit, name='done'),
	path('results/', views.ResultsView.as_view(template_name='feedback/results_list.html'), name='results'),
	path('results/<str:receiver>/', views.ReceiverView.as_view(), name='details'),
	path('review/', views.ReviewView.as_view(template_name='feedback/review_list.html'), name='review'),
	path('my/', views.MyView.as_view(template_name='feedback/my_list.html'), name='my'),
	path('edit/<int:id>', views.edit, name='edit'),
]
