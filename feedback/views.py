from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseNotFound
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth import logout
from django.contrib import messages
from django.views.generic.list import ListView
from django.utils import timezone

from people.models import User, Department, Team
from .models import Response
from .forms import ResponseForm

#XXX not sure I like how we use is_curator as a context var to in the base.html menu
def user_is_curator(user):
    return user.is_staff or Team.objects.filter(reviewer=user.id).exists()

@login_required
def index(request):
    giver = request.user
    teams = Team.objects.filter(people__in=[giver])

    receivers = User.objects.none()
    for team in teams:
        receivers = receivers.union(team.people.exclude(username=giver.username))

    if Department.objects.filter(lead=giver.id):
        receivers = receivers.union(User.objects.filter(department=giver.department).exclude(username=giver.username))
    
    others = User.objects.filter(is_active=True).exclude(username=giver.username).difference(receivers).order_by('first_name')

    context = {'receivers': receivers, 'giver' : giver, 'others': others }
    context['reviewer'] = user_is_curator(request.user)
    return render(request, 'feedback/index.html', context)

@login_required
def submit(request):
    user = request.user
    for item in request.POST:
        receiver = User.objects.filter(username=item)
        if receiver:
            anon = item + "_anon"
            if anon in request.POST:
                is_anon = True
            else:
                is_anon = False
            if request.POST[item] != '':
                response = Response(giver=user, receiver=receiver[0],text=request.POST[item], is_anon=is_anon, timestamp=timezone.now())
                response.save()

    # should we have a try here
    context = {'giver' : user}
    context['reviewer'] = user_is_curator(user)
    return render(request, 'feedback/done.html', context)

#XXX I'm using is_staff as the field for the people who can see all the feedback from everyone
def responses_to_review(reviewer):
    responses = Response.objects.none()
    for response in Response.objects.filter(ready=False):
        if Team.objects.filter(people__in=[response.giver,response.receiver], reviewer=reviewer).exists() or reviewer.is_staff:
            responses = responses.union(Response.objects.filter(pk=response.id))
    return responses

class ResultsView(ListView):
    model = User

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['pending'] = responses_to_review(self.request.user).count()
        context['reviewer'] = user_is_curator(self.request.user)
        return context

    def get_queryset(self):
        people = User.objects.none()
        for response in Response.objects.filter(ready=True):
                people = people.union(User.objects.filter(pk=response.receiver.id))
        return people

    def dispatch(self, request, *args, **kwargs):
        if not user_is_curator(request.user):
            messages.error(request, 'You do not have the permission.')
            return HttpResponseNotFound('<h1>Page not found</h1>')
        return super(ResultsView, self).dispatch(request,*args, **kwargs)

class ReceiverView(ListView):
    model = Response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['receiver'] = self.receiver
        context['reviewer'] = user_is_curator(self.resquest.user)
        return context
     
    def get_queryset(self):
        self.receiver = get_object_or_404(User, username=self.kwargs['receiver'])
        return Response.objects.filter(receiver=self.receiver.id)

    def dispatch(self, request, *args, **kwargs):
        if not user_is_curator(request.user):
            messages.error(request, 'You do not have the permission.')
            return HttpResponseNotFound('<h1>Page not found</h1>')
        return super(ReceiverView, self).dispatch(request,*args, **kwargs)

class ReviewView(ListView):
    model = Response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['reviewer'] = user_is_curator(self.request.user)
        return context
     
    def get_queryset(self):
        return responses_to_review(self.request.user)

    def post(self, request, *args, **kwargs):
        # save feedback edit
        if request.POST:
            form = ResponseForm(request.POST)
            if form.is_valid():
                #XXX is there a better way to get the reponse id?
                response = Response.objects.filter(pk=request.POST['id'])[0]
                response.text = form.cleaned_data['text']
                response.is_anon = form.cleaned_data['is_anon']
                response.save()
        
        return redirect('/feedback/review/')

    def dispatch(self, request, *args, **kwargs):
        if not user_is_curator(request.user):
            messages.error(request, 'You do not have the permission.')
            return HttpResponseNotFound('<h1>Page not found</h1>')
        
        # approve response #XXX this could be done better - in REST?
        if request.GET and 'id' in request.GET.keys():
            response = Response.objects.filter(pk=request.GET['id'])[0]
            response.ready = True
            response.save()

        return super(ReviewView, self).dispatch(request, *args, **kwargs)

class MyView(ListView):
    model = Response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['reviewer'] = user_is_curator(self.request.user)
        return context
     
    def get_queryset(self):
        return Response.objects.filter(receiver=self.request.user.id,ready=True)

    def dispatch(self, request, *args, **kwargs):
        # approve response #XXX this could be done better - in REST?
        if request.GET and 'id' in request.GET.keys():
            response = Response.objects.filter(pk=request.GET['id'])[0]
            if (request.user == response.receiver):
                response.public = not response.public
                response.save()
        
        return super(MyView, self).dispatch(request, *args, **kwargs)

@login_required
def edit(request, id):
    if not user_is_curator(request.user):
        return HttpResponseNotFound('<h1>Page not found</h1>')

    response = Response.objects.filter(pk=id)[0]

    form = ResponseForm()
    form.fields['text'].initial = response.text
    form.fields['is_anon'].initial = response.is_anon

    context = {'response': response, 'form' : form}
    context['reviewer'] = user_is_curator(request.user)
    return render(request, 'feedback/edit.html', context)

