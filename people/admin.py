from django.contrib import admin
from .models import Team, Department, User

admin.site.register(Team)
admin.site.register(Department)
admin.site.register(User)
