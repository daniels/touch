from tastypie.resources import ModelResource
from tastypie.authentication import ApiKeyAuthentication

from people.models import User

class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        fields = ['first_name', 'last_name']
        limit = 200
        authentication = ApiKeyAuthentication()