To run the project install the requirements on your enviroment:

pip install -r requirements.txt

and add a local_settings.py file to the root of your project:

---------------
#SECURITY WARNING: keep the secret key used in production secret!

SECRET_KEY = '******'

#SECURITY WARNING: don't run with debug turned on in production!

DEBUG = True

#Phabricator API url

API_URL = 'https://phabricator.collabora.com/'

SOCIAL_AUTH_PHABRICATOR_KEY = '****'

SOCIAL_AUTH_PHABRICATOR_SECRET = '****'

----------------
