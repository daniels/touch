#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
 
from django.core.wsgi import get_wsgi_application
from django.utils import timezone
from django.conf import settings
 
# derive location to your django project setting.py
proj_path = os.path.split(os.path.abspath(os.path.dirname(__file__)))[0]
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "touch.settings")
sys.path.append(proj_path)

# load your settings.py
os.chdir(proj_path)
 
# In essence you are actually loading up all the django components and settings
# so we gain access to djangos ORM
application = get_wsgi_application()

from people.models import User
from tastypie.models import ApiKey
import string
import random
import json

with open(sys.argv[1]) as f:
    data = json.load(f)

for u in data['objects']:
	if u['is_active'] == False:
		continue

	if u['login'] == 'breathesvc':
		continue

	if User.objects.filter(username=u['login']).exists():
		continue

	if u['login'] == 'tester':
		u['login'] = 'ocrete'
	
	is_staff = (u['login'] == 'padovan')

	user = User.objects.create(username=u['login'], first_name=u['forename'], last_name=u['surname'], is_staff=is_staff, is_superuser=is_staff)
	key = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)])
	ApiKey.objects.create(key=key, user=user)
	print("added", user.username)
