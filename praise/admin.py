from django.contrib import admin
from .models import Praise


class PraiseAdmin(admin.ModelAdmin):
    list_display = ('giver', 'receiver')
    search_fields = ('giver', 'receiver')

admin.site.register(Praise)
