from django import forms
from people.models import User

class PraiseForm(forms.Form):
    receiver = forms.ChoiceField(required=False)
    text = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Write kudos they deserve here...'}), max_length=1000, label="Praise")
