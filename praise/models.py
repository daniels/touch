from django.db import models
from people.models import User
from datetime import datetime

# Create your models here.
class Praise(models.Model):
    giver = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name="praise_giver")
    receiver = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name="praise_receiver")
    text = models.CharField(max_length=1000)
    timestamp = models.DateTimeField('timestamp', default=datetime.now)
    client = models.CharField(max_length=100, default='')
    def __str__(self):
        return self.giver.username + "-" + self.receiver.username
