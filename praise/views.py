from django.shortcuts import render
from django.http import StreamingHttpResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.conf import settings
import requests
import json

from .models import Praise
from people.models import User
from .forms import PraiseForm
from feedback.views import user_is_curator

def post_kudos_url(giver, receiver, text):
    headers = {'Content-type': 'application/json'}
    post_data = {}
    post_data['icon_url'] = "https://touch.collabora.com/static/img/favicon.ico"
    post_data['text'] = ":tada: :tada: :tada: [**kudos!**](https://touch.collabora.com/praise/) - from @" + giver.username + " to :medal_sports: **@" + receiver.username + "** \n>" + text
    r = requests.post(settings.KUDOS_WEBHOOK, json.dumps(post_data), headers=headers)

def choices_list(giver):
    l = []
    for u in User.objects.exclude(username=giver.username).filter(is_active=True).order_by('first_name'):
        l.append(tuple((u.id, u.name)))
    return l

@login_required
def index(request):
    giver = request.user

    if request.method=='POST':
        form = PraiseForm(request.POST)
        form.fields['receiver'].choices = choices_list(giver)
        if form.is_valid():
             receiver = User.objects.filter(id=form.cleaned_data['receiver'])[0]
             praise = Praise(giver=giver, receiver=receiver, text=form.cleaned_data["text"], timestamp=timezone.now(), client="web")
             praise.save()

             post_kudos_url(giver, receiver, form.cleaned_data["text"])
             form = PraiseForm()
    else:
        form = PraiseForm()

    kudos = Praise.objects.order_by('timestamp').reverse()
    form.fields['receiver'].choices = choices_list(giver)

    context =  {'kudos': kudos, 'form': form, 'reviewer' : user_is_curator(request.user) }
    return render(request, 'praise/index.html', context)
